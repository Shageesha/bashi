import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisplayStocksComponent } from './components/display-stocks/display-stocks.component';

const routes: Routes = [
  { path: '', component:  DisplayStocksComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StocksRoutingModule { }
