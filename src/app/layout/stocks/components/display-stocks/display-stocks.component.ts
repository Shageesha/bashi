import { Component, OnInit } from '@angular/core';
import { parseString } from 'xml2js'

@Component({
	selector: 'app-display-stocks',
	templateUrl: './display-stocks.component.html',
	styleUrls: ['./display-stocks.component.scss']
})
export class DisplayStocksComponent implements OnInit {

	stockList: any;
	stockListFinal:any;

	constructor() { }

	ngOnInit() {
		this.convertToJson();

		this.stockList = [this.convertToJson()];
		console.log("Final output",this.stockList);

		this.stockListFinal = this.stockList[0].Entries.Entry;
		console.log(this.stockListFinal);
		
	}

	convertToJson() {
		// let parseString = x.parseString;
		let fresult;
		// let xml = "<Entry><product_id>120</product_id><product_name>captain-crunch</product_name><avail_stock>1000</avail_stock><stock_location>storage</stock_location></Entry><Entry><product_id>120</product_id><product_name>captain-crunch</product_name><avail_stock>1000</avail_stock><stock_location>storage</stock_location></Entry><Entry><product_id>120</product_id><product_name>captain-crunch</product_name><avail_stock>1000</avail_stock><stock_location>storage</stock_location></Entry><Entry><product_id>120</product_id><product_name>captain-crunch</product_name><avail_stock>1000</avail_stock><stock_location>storage</stock_location></Entry>";
		let xml ="<Entries><Entry><product_id>120</product_id><product_name>captain-crunch</product_name><avail_stock>1000</avail_stock><stock_location>storage</stock_location></Entry><Entry><product_id>122</product_id><product_name>rice</product_name><avail_stock>900</avail_stock><stock_location>storage</stock_location></Entry><Entry><product_id>123</product_id><product_name>pizza</product_name><avail_stock>100</avail_stock><stock_location>store</stock_location></Entry><Entry><product_id>124</product_id>    <product_name>milk</product_name>    <avail_stock>900</avail_stock>    <stock_location>storage</stock_location></Entry><Entry>    <product_id>125</product_id>    <product_name>oreo</product_name>    <avail_stock>100</avail_stock>    <stock_location>storage</stock_location></Entry><Entry>    <product_id>126</product_id>    <product_name>Gummy-Bears</product_name>    <avail_stock>100</avail_stock>    <stock_location>storage</stock_location></Entry><Entry>    <product_id>127</product_id>    <product_name>Crushers</product_name>    <avail_stock>1111</avail_stock>    <stock_location>store</stock_location></Entry><Entry>    <product_id>128</product_id>    <product_name>Beer</product_name>    <avail_stock>100000</avail_stock>    <stock_location>store</stock_location></Entry><Entry>    <product_id>300</product_id>    <product_name>Crushers</product_name>    <avail_stock>222</avail_stock>    <stock_location>store</stock_location></Entry><Entry>    <product_id>301</product_id>    <product_name>Dominos Pizza</product_name>    <avail_stock>5000</avail_stock>    <stock_location>store</stock_location></Entry></Entries>"
		parseString(xml, function (err, result) {
			// console.log(result);
			fresult = result
		});
		return fresult;
	}


}
