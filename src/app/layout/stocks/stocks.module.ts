import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StocksRoutingModule } from './stocks-routing.module';
import { DisplayStocksComponent } from './components/display-stocks/display-stocks.component';

@NgModule({
  declarations: [DisplayStocksComponent],
  imports: [
    CommonModule,
    StocksRoutingModule
  ]
})
export class StocksModule { }
